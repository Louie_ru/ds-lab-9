# DS Lab 9

# /etc/hosts
```
172.31.41.216 node1
172.31.36.164 node2
172.31.38.129 node3
```

# initiate
```javascript
rs.initiate( {
   _id : "rs0",
   members: [
      { _id: 0, host: "node1:27017" },
      { _id: 1, host: "node2:27017" },
      { _id: 2, host: "node3:27017" }
   ]
})
```

# Before node1 shutdown

![before](https://gitlab.com/Louie_ru/ds-lab-9/raw/master/before.png)

```javascript
rs.status()
{
	"set" : "rs0",
	"date" : ISODate("2019-10-29T22:31:21.263Z"),
	"myState" : 1,
	"term" : NumberLong(1),
	"syncingTo" : "",
	"syncSourceHost" : "",
	"syncSourceId" : -1,
	"heartbeatIntervalMillis" : NumberLong(2000),
	"majorityVoteCount" : 2,
	"writeMajorityCount" : 2,
	"optimes" : {
		"lastCommittedOpTime" : {
			"ts" : Timestamp(1572388273, 1),
			"t" : NumberLong(1)
		},
		"lastCommittedWallTime" : ISODate("2019-10-29T22:31:13.629Z"),
		"readConcernMajorityOpTime" : {
			"ts" : Timestamp(1572388273, 1),
			"t" : NumberLong(1)
		},
		"readConcernMajorityWallTime" : ISODate("2019-10-29T22:31:13.629Z"),
		"appliedOpTime" : {
			"ts" : Timestamp(1572388273, 1),
			"t" : NumberLong(1)
		},
		"durableOpTime" : {
			"ts" : Timestamp(1572388273, 1),
			"t" : NumberLong(1)
		},
		"lastAppliedWallTime" : ISODate("2019-10-29T22:31:13.629Z"),
		"lastDurableWallTime" : ISODate("2019-10-29T22:31:13.629Z")
	},
	"lastStableRecoveryTimestamp" : Timestamp(1572388254, 1),
	"lastStableCheckpointTimestamp" : Timestamp(1572388254, 1),
	"electionCandidateMetrics" : {
		"lastElectionReason" : "electionTimeout",
		"lastElectionDate" : ISODate("2019-10-29T21:10:52.822Z"),
		"termAtElection" : NumberLong(1),
		"lastCommittedOpTimeAtElection" : {
			"ts" : Timestamp(0, 0),
			"t" : NumberLong(-1)
		},
		"lastSeenOpTimeAtElection" : {
			"ts" : Timestamp(1572383442, 1),
			"t" : NumberLong(-1)
		},
		"numVotesNeeded" : 2,
		"priorityAtElection" : 1,
		"electionTimeoutMillis" : NumberLong(10000),
		"numCatchUpOps" : NumberLong(27017),
		"newTermStartDate" : ISODate("2019-10-29T21:10:53.487Z"),
		"wMajorityWriteAvailabilityDate" : ISODate("2019-10-29T21:10:54.349Z")
	},
	"members" : [
		{
			"_id" : 0,
			"name" : "node1:27017",
			"ip" : "172.31.41.216",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			"uptime" : 4882,
			"optime" : {
				"ts" : Timestamp(1572388273, 1),
				"t" : NumberLong(1)
			},
			"optimeDate" : ISODate("2019-10-29T22:31:13Z"),
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "",
			"electionTime" : Timestamp(1572383452, 1),
			"electionDate" : ISODate("2019-10-29T21:10:52Z"),
			"configVersion" : 1,
			"self" : true,
			"lastHeartbeatMessage" : ""
		},
		{
			"_id" : 1,
			"name" : "node2:27017",
			"ip" : "172.31.36.164",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 4838,
			"optime" : {
				"ts" : Timestamp(1572388273, 1),
				"t" : NumberLong(1)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1572388273, 1),
				"t" : NumberLong(1)
			},
			"optimeDate" : ISODate("2019-10-29T22:31:13Z"),
			"optimeDurableDate" : ISODate("2019-10-29T22:31:13Z"),
			"lastHeartbeat" : ISODate("2019-10-29T22:31:19.852Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T22:31:20.754Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "node1:27017",
			"syncSourceHost" : "node1:27017",
			"syncSourceId" : 0,
			"infoMessage" : "",
			"configVersion" : 1
		},
		{
			"_id" : 2,
			"name" : "node3:27017",
			"ip" : "172.31.38.129",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 4838,
			"optime" : {
				"ts" : Timestamp(1572388273, 1),
				"t" : NumberLong(1)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1572388273, 1),
				"t" : NumberLong(1)
			},
			"optimeDate" : ISODate("2019-10-29T22:31:13Z"),
			"optimeDurableDate" : ISODate("2019-10-29T22:31:13Z"),
			"lastHeartbeat" : ISODate("2019-10-29T22:31:19.845Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T22:31:20.739Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "node1:27017",
			"syncSourceHost" : "node1:27017",
			"syncSourceId" : 0,
			"infoMessage" : "",
			"configVersion" : 1
		}
	],
	"ok" : 1,
	"$clusterTime" : {
		"clusterTime" : Timestamp(1572388273, 1),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	},
	"operationTime" : Timestamp(1572388273, 1)
}

rs.config()
{
	"_id" : "rs0",
	"version" : 1,
	"protocolVersion" : NumberLong(1),
	"writeConcernMajorityJournalDefault" : true,
	"members" : [
		{
			"_id" : 0,
			"host" : "node1:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {

			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 1,
			"host" : "node2:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {

			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 2,
			"host" : "node3:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {

			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		}
	],
	"settings" : {
		"chainingAllowed" : true,
		"heartbeatIntervalMillis" : 2000,
		"heartbeatTimeoutSecs" : 10,
		"electionTimeoutMillis" : 10000,
		"catchUpTimeoutMillis" : -1,
		"catchUpTakeoverDelayMillis" : 30000,
		"getLastErrorModes" : {

		},
		"getLastErrorDefaults" : {
			"w" : 1,
			"wtimeout" : 0
		},
		"replicaSetId" : ObjectId("5db8aad213fe5225705d4a22")
	}
}
```

# After node1 shutdown

![after](https://gitlab.com/Louie_ru/ds-lab-9/raw/master/after.png)

```javascript
rs.status()
{
	"set" : "rs0",
	"date" : ISODate("2019-10-29T22:41:09.449Z"),
	"myState" : 1,
	"term" : NumberLong(2),
	"syncingTo" : "",
	"syncSourceHost" : "",
	"syncSourceId" : -1,
	"heartbeatIntervalMillis" : NumberLong(2000),
	"majorityVoteCount" : 2,
	"writeMajorityCount" : 2,
	"optimes" : {
		"lastCommittedOpTime" : {
			"ts" : Timestamp(1572388858, 1),
			"t" : NumberLong(2)
		},
		"lastCommittedWallTime" : ISODate("2019-10-29T22:40:58.810Z"),
		"readConcernMajorityOpTime" : {
			"ts" : Timestamp(1572388858, 1),
			"t" : NumberLong(2)
		},
		"readConcernMajorityWallTime" : ISODate("2019-10-29T22:40:58.810Z"),
		"appliedOpTime" : {
			"ts" : Timestamp(1572388858, 1),
			"t" : NumberLong(2)
		},
		"durableOpTime" : {
			"ts" : Timestamp(1572388858, 1),
			"t" : NumberLong(2)
		},
		"lastAppliedWallTime" : ISODate("2019-10-29T22:40:58.810Z"),
		"lastDurableWallTime" : ISODate("2019-10-29T22:40:58.810Z")
	},
	"lastStableRecoveryTimestamp" : Timestamp(1572388850, 1),
	"lastStableCheckpointTimestamp" : Timestamp(1572388850, 1),
	"electionCandidateMetrics" : {
		"lastElectionReason" : "stepUpRequestSkipDryRun",
		"lastElectionDate" : ISODate("2019-10-29T22:39:49.972Z"),
		"termAtElection" : NumberLong(2),
		"lastCommittedOpTimeAtElection" : {
			"ts" : Timestamp(1572388783, 1),
			"t" : NumberLong(1)
		},
		"lastSeenOpTimeAtElection" : {
			"ts" : Timestamp(1572388783, 1),
			"t" : NumberLong(1)
		},
		"numVotesNeeded" : 2,
		"priorityAtElection" : 1,
		"electionTimeoutMillis" : NumberLong(10000),
		"priorPrimaryMemberId" : 0,
		"numCatchUpOps" : NumberLong(27017),
		"newTermStartDate" : ISODate("2019-10-29T22:39:50.639Z"),
		"wMajorityWriteAvailabilityDate" : ISODate("2019-10-29T22:39:52.023Z")
	},
	"members" : [
		{
			"_id" : 0,
			"name" : "node1:27017",
			"ip" : "172.31.41.216",
			"health" : 0,
			"state" : 8,
			"stateStr" : "(not reachable/healthy)",
			"uptime" : 0,
			"optime" : {
				"ts" : Timestamp(0, 0),
				"t" : NumberLong(-1)
			},
			"optimeDurable" : {
				"ts" : Timestamp(0, 0),
				"t" : NumberLong(-1)
			},
			"optimeDate" : ISODate("1970-01-01T00:00:00Z"),
			"optimeDurableDate" : ISODate("1970-01-01T00:00:00Z"),
			"lastHeartbeat" : ISODate("2019-10-29T22:41:07.817Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T22:39:49.885Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "Error connecting to node1:27017 (172.31.41.216:27017) :: caused by :: No route to host",
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "",
			"configVersion" : -1
		},
		{
			"_id" : 1,
			"name" : "node2:27017",
			"ip" : "172.31.36.164",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			"uptime" : 5469,
			"optime" : {
				"ts" : Timestamp(1572388858, 1),
				"t" : NumberLong(2)
			},
			"optimeDate" : ISODate("2019-10-29T22:40:58Z"),
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "",
			"electionTime" : Timestamp(1572388789, 1),
			"electionDate" : ISODate("2019-10-29T22:39:49Z"),
			"configVersion" : 1,
			"self" : true,
			"lastHeartbeatMessage" : ""
		},
		{
			"_id" : 2,
			"name" : "node3:27017",
			"ip" : "172.31.38.129",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 5426,
			"optime" : {
				"ts" : Timestamp(1572388858, 1),
				"t" : NumberLong(2)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1572388858, 1),
				"t" : NumberLong(2)
			},
			"optimeDate" : ISODate("2019-10-29T22:40:58Z"),
			"optimeDurableDate" : ISODate("2019-10-29T22:40:58Z"),
			"lastHeartbeat" : ISODate("2019-10-29T22:41:07.987Z"),
			"lastHeartbeatRecv" : ISODate("2019-10-29T22:41:08.033Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "node2:27017",
			"syncSourceHost" : "node2:27017",
			"syncSourceId" : 1,
			"infoMessage" : "",
			"configVersion" : 1
		}
	],
	"ok" : 1,
	"$clusterTime" : {
		"clusterTime" : Timestamp(1572388858, 1),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	},
	"operationTime" : Timestamp(1572388858, 1)
}

rs.config()
{
	"_id" : "rs0",
	"version" : 1,
	"protocolVersion" : NumberLong(1),
	"writeConcernMajorityJournalDefault" : true,
	"members" : [
		{
			"_id" : 0,
			"host" : "node1:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {

			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 1,
			"host" : "node2:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {

			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		},
		{
			"_id" : 2,
			"host" : "node3:27017",
			"arbiterOnly" : false,
			"buildIndexes" : true,
			"hidden" : false,
			"priority" : 1,
			"tags" : {

			},
			"slaveDelay" : NumberLong(0),
			"votes" : 1
		}
	],
	"settings" : {
		"chainingAllowed" : true,
		"heartbeatIntervalMillis" : 2000,
		"heartbeatTimeoutSecs" : 10,
		"electionTimeoutMillis" : 10000,
		"catchUpTimeoutMillis" : -1,
		"catchUpTakeoverDelayMillis" : 30000,
		"getLastErrorModes" : {

		},
		"getLastErrorDefaults" : {
			"w" : 1,
			"wtimeout" : 0
		},
		"replicaSetId" : ObjectId("5db8aad213fe5225705d4a22")
	}
}
```
