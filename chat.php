<!DOCTYPE html>
<html lang="en">
<head>
	<title>NoSQL Chat</title>
	<meta charset="UTF-8">

	<div class="rainbow-text">
	<span style="color:#ff4800">B</span><span style="color:#ff9100">E</span><span style="color:#ffda00">S</span><span style="color:#daff00">T</span> <span style="color:#48ff00">C</span><span style="color:#00ff00">H</span><span style="color:#00ff48">A</span><span style="color:#00ff91">T</span> <span style="color:#00daff">D</span><span style="color:#0091ff">E</span><span style="color:#0048ff">S</span><span style="color:#0000ff">I</span><span style="color:#4800ff">G</span><span style="color:#9100ff">N</span> <span style="color:#ff00da">E</span><span style="color:#ff0091">V</span><span style="color:#ff0048">E</span><span style="color:#ff0000">R</span>
	</div>

<style>
body{
	background-image : url("https://cdn.pixabay.com/photo/2015/06/18/01/46/hack-813290_960_720.jpg");
	color: green;
}
</style>

</head>

<body>


<?php
		// connect to mongodb
		$m = new MongoClient();
		// select database
		$db = $m->chat;
		// select collection
		$messages = $db->messages;

	  if(isset($_POST['msg'])){
	     // make new document
	     $document = array(
	        "text" => $_POST['msg']
	     );
	     // insert new document
	     $messages->insert($document);
	  }


   $cursor = $messages->find();
   // iterate cursor to display text of messages
   foreach ($cursor as $document) {
      echo $document["text"] . "<br>"; //XSS found!
   }
?>

<br>
<br>
<form method="POST">
  Enter message: <input type="text" name="msg">
  <input type="submit" value="Submit">
</form>


</body>
</html>
